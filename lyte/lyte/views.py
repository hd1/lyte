from collections import defaultdict
import datetime
import json
import logging

from django.core.serializers import serialize
from django.http import JsonResponse
from django.http.response import HttpResponseNotAllowed
from django.views.decorators.csrf import csrf_exempt
from . import models

@csrf_exempt
def update(request):
    if request.method != 'POST':
        return HttpResponseNotAllowed(request.method)
    event_id = request.POST.get('id')
    target = models.Event.objects.get(pk=int(event_id))

    if 'name' in request.POST:
        target.name = request.POST.get('name')
    if 'org' in request.POST:
        target.organizer = request.POST.get('org')
    if 'cost' in request.POST:
        target.ticket_cost = request.POST.get('cost')
    
    target.save()
    return JsonResponse(serialize('json', [target]), safe=False)

@csrf_exempt
def search(request):
    if request.method != 'GET':
        return HttpResponseNotAllowed(request.method)

    search_dict = defaultdict((lambda: 'x'))

    res = []

    for o in models.Event.objects.all():
        if o.organizer:
           res.append({'name': o.name, 'date': o.start_date.strftime('%c'), 'organizer': o.organizer.replace(r'\\',''), 'cost': o.ticket_cost}) 
        else:
           res.append({'name': o.name, 'date': o.start_date.strftime('%c'), 'organizer': '', 'cost': o.ticket_cost}) 
            

    if search_dict['org'] != 'x':
        res = [r for r in res if search_dict['org'] in r['organizer']]

    if search_dict['name'] != 'x':
        res = [r for r in res if search_dict['name'] in r['name']]

    if search_dict['date'] != 'x':
        res = [r for r in res if r.start_date == search_dict['date']]
    
    if search_dict['cost'] != 'x':
        res = [r for r in res if r.ticket_cost < float(search_dict['cost'])]

    return JsonResponse(res, safe=False)

