import datetime
from django.http import JsonResponse
from django.test import Client, TestCase
import json
from . import models

class LyteTestCase(TestCase):
    def test_find(self):
        e = models.Event.objects.first()
        client = Client()
        resp = json.loads(client.get('/search', {'name': e.name}).content.decode('utf-8'))
        expected = list(models.Event.objects.filter(name=e.name).values())
        for e in expected:
            del e['id'] 
            e['date'] = e['start_date'].strftime('%c')
            del e['start_date']
            e['cost'] = e['ticket_cost']
            del e['ticket_cost']
        self.assertEquals(sorted(list(expected[0].keys())), sorted(list(resp[0].keys())))

    def test_update(self):
        e = models.Event.objects.first()
        client = Client()
        client.post('/update', {'id': e.id, 'name': e.name+'s'})
        e = models.Event.objects.first()
        self.assertTrue(e.name.endswith('s'))
    
    def test_find_only_allows_get_method(self):
        query_url = '/search?org=UEFA'
        client = Client()
        self.assertEquals(405, client.post(query_url).status_code)
        self.assertEquals(405, client.put(query_url).status_code)
        self.assertEquals(405, client.delete(query_url).status_code)

    def test_update_only_allows_post_method(self):
        update_url = '/update'
        client = Client()
        self.assertEquals(405, client.put(update_url).status_code)
        self.assertEquals(405, client.get(update_url).status_code)
        self.assertEquals(405, client.delete(update_url).status_code)

