import csv
import io
import json
import logging
import os
import sqlite3

from django.db import models, transaction
from eventbrite import Eventbrite
from lyte import settings
import requests

class Event(models.Model):
    # event name, event start date, organizer name, ticket cost
    name = models.CharField(max_length=30)
    start_date = models.DateField()
    organizer = models.CharField(max_length=1024, default='\\', null=True)
    ticket_cost = models.CharField(max_length=32, default=0)
    
    def __str__(self):
        return '%s was organized by %s on %s and costs %s' % (self.name, self.organizer, self.start_date, self.ticket_cost,)


# loads today's eventbrite events for San Francisco into SQLite3
def load():
    logging.basicConfig(level=logging.DEBUG)
    OAUTH_TOKEN = 'BZUF4DDBGPUZRAOUXFKC'
    data = {'location.latitude': 37.7749, 'location.longitude': -122.4194, 'start_date.keyword': 'today', 'location.within': '61km', 'expand': 'organizer'}
    api = Eventbrite(OAUTH_TOKEN)
    total = 0
    for e in api.event_search(**data)['events']:
        headers={'Authorization': 'Bearer {0}'.format(api.oauth_token)}
        costs = io.StringIO()
        w = csv.writer(costs)
        costs_source = requests.get('https://www.eventbriteapi.com/v3/events/{0}/ticket_classes/'.format(e['id']), headers={'Authorization': 'Bearer {0}'.format(api.oauth_token)}).json()
        cost_list = []
        for c in costs_source['ticket_classes']:

            if c['free']: 
                cost_list.append('0')
            else: 
                try:
                    cost_list.append(c['cost']['currency']+' '+c['cost']['major_value'])
                except KeyError:
                    cost_list.append('unknown')
        costs_val = ', '.join(cost_list)
        if not e['organizer']['description']['text']:
            e['organizer']['description']['text'] = e['organizer']['name']
        if not e['organizer']['description']['text']:
            e['organizer']['description']['text'] = 'No organizer information found'
        with transaction.atomic():
            evt = Event(name=e['name']['text'], start_date=e['start']['local'][:e['start']['local'].index('T')], organizer=e['organizer']['description']['text'].strip(), ticket_cost=costs_val)
            evt.save()
            total = total + 1
    print('{0} records imported'.format(total))


